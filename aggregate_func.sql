select count(employee_id) as count_per_dep, department_id from employees
group by department_id;

select sum(salary) as sum_sal_per_dep, department_id from employees 
group by department_id;


select min(e.salary), d.department_name from employees e
join departments d on e.department_id=d.department_id 
group by department_name;

select max(e.salary), d.department_name from employees e
join departments d on e.department_id=d.department_id 
group by department_name;

select round(avg(e.salary),2), d.department_name from employees e
join departments d on e.department_id=d.department_id 
group by department_name;

select median(e.salary), d.department_name from employees e
join departments d on e.department_id=d.department_id 
group by department_name;
