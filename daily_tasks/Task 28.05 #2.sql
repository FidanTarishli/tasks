--1. Theoretical: Explain the purpose of the GROUP BY clause in SQL.

--    The GROUP BY clause in SQL is quite useful tool for analyzing data by grouping rows of table according to its coloumns. Usually, in order to perform calculations on grouped data aggregate functions such as COUNT, SUM, AVG, MIN, MAX, etc. are used. To give an example, user can analyze total or average orders made by each client to improve sales and to know the needs of clients.

 --2. Incomplete - Practical: Write a SQL query to get the total salary paid by each department.

SELECT
    d.department_name,
    SUM(e.salary) AS total_salary
FROM
         employees e
    JOIN departments d ON e.department_id = d.department_id
GROUP BY
    department_name;

--1.  Problem-Solving: How would you use SQL to find the average salary of each department?

--  Solution is similar to the practical task given above. This time we will use average aggregate function instead of sum to find the average salary of each department, again group by will be used to group average salaries into rows of different departments.

SELECT
    d.department_name,
    AVG(e.salary) AS average_salary
FROM
         employees e
    JOIN departments d ON e.department_id = d.department_id
GROUP BY
    department_name;

--2. **Complete - Practical:** Write a SQL query to find the names and job titles of all employees who work in the 'IT' department.
    
    --*Hint: You will need to join employees and departments tables.*

SELECT
    e.first_name,
    j.job_title,
    d.department_name
FROM
         employees e
    JOIN jobs        j ON e.job_id = j.job_id
    JOIN departments d ON e.department_id = d.department_id
WHERE
    department_name = 'IT';

 --3.   Incomplete - Practical: Write a SQL query to list all job titles and the highest salary within each title.

SELECT
    j.job_title,
    MAX(e.salary) AS max_salary
FROM
         jobs j
    JOIN employees e ON j.job_id = e.job_id
GROUP BY
    j.job_title;
--task


SELECT
    MAX(e.salary) AS highest_salary,
    j.job_title
FROM
         employees e
    JOIN jobs j ON e.job_id = j.job_id
GROUP BY
    j.job_title
ORDER BY
    1 DESC;
--my version














