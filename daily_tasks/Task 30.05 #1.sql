--Group 1 - INNER JOIN, LEFT JOIN, RIGHT JOIN:

--Task 1:Description: Retrieve the employee ID, first name, department name, job title, and country name of all employees who have a department assigned. Include employees who don't have a job title or a country assigned.

SELECT
    e.employee_id,
    e.first_name,
    d.department_name,
    j.job_title,
    c.country_name
FROM
    employees   e
    LEFT JOIN departments d ON e.department_id = d.department_id
    JOIN jobs        j ON e.job_id = j.job_id
    JOIN locations   l ON l.location_id = d.location_id
    JOIN countries   c ON c.country_id = l.country_id;

--Task 2:Description: Retrieve the employee ID, first name, department name, job title, and country name of all employees along with their job history, if available. Include employees who dont have a department or job history.

SELECT
    e.employee_id,
    e.first_name,
    d.department_name,
    j.job_title,
    c.country_name,
    jh.start_date,
    jh.end_date
FROM
    employees   e
    LEFT JOIN departments d ON e.department_id = d.department_id
    LEFT JOIN jobs        j ON e.job_id = e.job_id
    LEFT JOIN job_history jh ON e.employee_id = jh.employee_id
    JOIN locations   l ON d.location_id = l.location_id
    JOIN countries   c ON l.country_id = c.country_id;

--Task 3:Description: Retrieve the employee ID, first name, department name, job title, and country name of all employees along with their corresponding department information. Include departments that don't have any employees assigned.

SELECT
    e.employee_id,
    e.first_name,
    d.*,
    j.job_title,
    c.country_name
FROM
    employees   e
    RIGHT JOIN departments d ON e.department_id = d.department_id
    RIGHT JOIN jobs        j ON e.job_id = j.job_id
    JOIN locations   l ON l.location_id = d.location_id
    JOIN countries   c ON c.country_id = l.country_id;


--Task 4:Description: Retrieve the employee ID, first name, department name, job title, and country name of all employees along with their corresponding job title and country information. Include employees who don't have a department or job title.

SELECT
    e.employee_id,
    e.first_name,
    d.department_name,
    j.job_title,
    c.*
FROM
         employees e
    LEFT JOIN departments d ON e.department_id = d.department_id
    LEFT JOIN jobs        j ON e.job_id = j.job_id
    INNER JOIN locations   l ON l.location_id = d.location_id
    INNER JOIN countries   c ON c.country_id = l.country_id; 

--Task 5:Description: Retrieve the employee ID, first name, department name, job title, and country name of all employees along with their corresponding department and job title information. Include employees who don't have a country assigned.

SELECT
    e.employee_id,
    e.first_name,
    d.*,
    j.job_title,
    c.country_name
FROM
         employees e
    JOIN departments d ON e.department_id = d.department_id
    JOIN jobs        j ON e.job_id = j.job_id
    JOIN locations   l ON l.location_id = d.location_id
    LEFT JOIN countries   c ON c.country_id = l.country_id;  

















    

