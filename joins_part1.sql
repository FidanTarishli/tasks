select e.first_name, e.last_name, d.department_name
from employees e
join departments d
on e.department_id=d.department_id;


select e.first_name as emp_name,e.salary as emp_salary, m.first_name as man_name, m.salary as man_salary
from employees e
join employees m
on e.manager_id=m.manager_id;

select * from employees 
natural join departments ;


select * from employees 
natural join departments 
using (department_id);
