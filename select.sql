select * from employees;

select first_name from employees where employee_id>100;

select sum(salary), first_name from employees
group by first_name;

select sum(salary), first_name from employees
group by first_name having sum(salary)>4000;

select sum(salary), first_name from employees
group by first_name
order by sum(salary) desc;

select sum(salary), first_name from employees
group by first_name
order by sum(salary) desc
fetch first 2 rows only;
