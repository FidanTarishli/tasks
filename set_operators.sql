select department_id from employees
union
select department_id from departments;

select department_id from employees
union all
select department_id from departments;

select department_id from employees
intersect
select department_id from departments;

select department_id from departments
minus
select department_id from employees;


select avg(salary) from employees group by employee_id
union
select salary from employees;

select avg(salary) from employees group by employee_id
union all
select salary from employees;


select salary from employees 
intersect
select avg(salary) from employees group by employee_id;
