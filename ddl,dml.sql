create table customers(
customer_id int primary key,
first_name varchar(100) NOT NULL,
last_name varchar(100) NOT NULL,
address varchar(100),
phone varchar (100)
);

ALTER TABLE customers rename column phone to phone1;

drop table customers;


insert into customers values(1,'Fidan','Tarishli','1234 street','123456');
commit;

update customers set customer_id=2 where customer_id=1;
commit;

delete from customers where customer_id=2;
commit;
