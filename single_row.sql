
select upper('hello') as UPPER from dual;

select lower ('HELLO') AS LOWER from dual;

select substr('hello world',7,5) from dual;

select initcap('hello world') from dual;

select concat('hello ', 'world') as FULL_STR from dual;

select round(3.14578,3) as round from dual;

select trunc(3.14578,3) as trunc from dual;

select abs(-6) as abs from dual;

select mod(15,6) as remainder from dual;

select power(2,4) as power from dual;

select sysdate from dual;

select current_timestamp as current_date_with_time from dual;

select first_name,extract(YEAR from hire_date) as HIRE_YEAR from employees;
