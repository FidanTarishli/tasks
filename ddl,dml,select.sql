create table customer2
(customer_id number,
first_name varchar2(50),
last_name varchar2(50),
email varchar2(50),
address varchar2(100));


alter table customer2
modify (email varchar2(100));

alter table customer2
add phone_number varchar2(15);

select * from customer2;

alter table customer2
add constraint pk_customer_id primary key(customer_id);

alter table customer2
add constraint uc_customer2 unique(email);

insert into customer2(customer_id,first_name,last_name,email,address)
values (4,'Fidan','Tarishli','fidan1234@','123 street');

update customer2 set phone_number='2345677' where customer_id=4;

delete from customer2 where first_name='Fidan';



select * from employees;

select first_name, last_name, salary from employees
where salary>3000;


select max(salary), department_id, first_name from employees
group by department_id, first_name;

select max(salary), department_id, first_name from employees
group by department_id, first_name having department_id<>80;

select max(salary), department_id, first_name from employees
group by department_id, first_name having department_id<>80
order by 1 desc;

select max(salary), department_id, first_name from employees
group by department_id, first_name having department_id<>80
order by 1 desc
fetch first 5 rows only;
